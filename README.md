# «2.1. Системы контроля версий» - Иван Мельников - DEVSYS-9

## Задание №1 - Создать и настроить репозиторий для дальнейшей работы на курсе
Описание шаблона файла .gitignore:

`**/.terraform/*`
Все файлы в каталоге _.terraform_

`*.tfstate
*.tfstate.*`
Файлы с расширением _.tfstate_, а также с префиксом в имени _.tfstate._

`crash.log`
Файл crash.log

`*.tfvars`
Файлы с расширением _.tfvars_

`override.tf
override.tf.json
*_override.tf
*_override.tf.json`
Файлы _override.tf_ и _override.tf.json_, а также с окончанием в имени __override.tf_ и __override.tf.json_

`.terraformrc
terraform.rc`
Файлы с расширением _.terraformrc_, а также файл _terraform.rc_

## Задание №2 – Знакомство с документаций
Выполнено.